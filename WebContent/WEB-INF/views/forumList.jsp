<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>List of forums</title>
<link rel="stylesheet" type="text/css" href="resources/forum.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$('#name').click(function() {
			var name = $(this).attr("class");

			$.ajax({
				type : "GET",
				url : "forumList/order/"+name,
				//data : "order=" + name,
				success : function(response) {
					// we have the response
					$('#content tbody').html(response);
			    	if (name == "asc")	name = "desc";
			    	else name = "asc";
			    	$('#name').attr("class", name);
				},
				error : function(e) {
					alert('Error: ' + e);
				}
			});

		});
	});
</script>
</head>
<body>
	<div class="navBar">
		<jsp:include page="navBar.jsp" />
	</div>
	<div id="content">
		<table width="100%" cellpadding="1" cellspacing="1">
			<thead>
				<tr>
					<th><a href="javascript:void(0)" id="name" class="asc"
						style="color: white;">Forum name</a></th>
					<th>Forum description</th>
					<th>Forum created</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="forum" items="${forumList}">
					<tr>
						<td><a href="<c:url value='/forum.html?id=${forum.id}'/>"><span
								class="name">${forum.name}</span></a></td>
						<td>${forum.description}</td>
						<td>${forum.created}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>