<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%---@ taglib prefix="security" uri="http://www.springframework.org/security/tags" --%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
  <head>
    <title><spring:message code="user.form"/></title>
  </head>
  <body>
  	<jsp:include page="navBar.jsp" />
    <h1><spring:message code="user.form"/></h1>
    <form:form commandName="user">
    <form:errors />
      <table>
	        <tr>
	          <th><form:label path="firstName"><spring:message code="user.firstName"/></form:label> *:</th>
	          <td>
	            <form:input path="firstName" />
	            <form:errors path="firstName" cssClass="error"/>
	          </td>
	        </tr>
	        <tr>
	          <th><form:label path="lastName"><spring:message code="user.lastName"/></form:label> *:</th>
	          <td>
	            <form:input path="lastName" />
	            <form:errors path="lastName" cssClass="error"/>
	          </td>
	        </tr>
	        <tr>
	          <th><form:label path="email"><spring:message code="user.email"/></form:label> *:</th>
	          <td>
	            <form:input path="email" />
	            <form:errors path="email" cssClass="error"/>
	          </td>
	        </tr>
	        <tr>
	          <th><form:label path="password"><spring:message code="user.password"/></form:label> *:</th>
	          <td>
	            <form:input path="password" />
	            <form:errors path="password" cssClass="error"/>
	          </td>
	        </tr>
        <tr>
          <th>&nbsp;</th>
          <td>
            <input type="submit" value="Save"/>
          </td>
        </tr>
      </table>
    </form:form>
  </body>
</html>