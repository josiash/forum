<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>List of categories</title>
    <link rel="stylesheet" type="text/css" href="resources/forum.css">
</head>
<body>
	<div class="navBar">
		<jsp:include page="navBar.jsp"/>
	</div>
	<div>
		<table width="100%" cellpadding="1" cellspacing="1">
			<tr>
				<th>Category name</th>
				<th>Category description</th>
				<th>Category created</th>
			</tr>
			<c:forEach var="category" items="${categoryList}" >
				<tr>
				    <td><a href="<c:url value='/category.html?id=${category.id}'/>"><span class="name">${category.name}</span></a></td>
				    <td>${category.description}</td>
				    <td>${category.created}</td>
			    </tr>
			</c:forEach>
		</table>	
	</div>
	<div class="category">
		<a href="<c:url value="categoryForm.html?forum_id=${forum.id}"/>">Add category</a>
	</div>
</body>
</html>