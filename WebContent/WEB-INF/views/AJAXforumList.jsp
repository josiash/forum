<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


			<c:forEach var="forum" items="${forumList}">
				<tr>
					<td><a href="<c:url value='/forum.html?id=${forum.id}'/>"><span
							class="name">${forum.name}</span></a></td>
					<td>${forum.description}</td>
					<td>${forum.created}</td>
				</tr>
			</c:forEach>

	