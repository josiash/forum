<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>List of topics in category</title>
    <link rel="stylesheet" type="text/css" href="resources/forum.css">
</head>
<body>
	<div class="navBar">
		<jsp:include page="navBar.jsp"/>
	</div>
	<div>
		<table width="100%" cellpadding="1" cellspacing="1">
			<tr>
				<th>Topic description</th>
				<th>Topic created</th>
			</tr>
				<tr>
				    <td>${topic.description}</td>
				    <td>${topic.created}</td>
			    </tr>
		</table>	
	</div>
	<div class="mT10">
		<a href="<c:url value="topicForm.html?category_id=${category.id}"/>">Add topic</a>
	</div>
</body>
</html>