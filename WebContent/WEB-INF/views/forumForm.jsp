<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%---@ taglib prefix="security" uri="http://www.springframework.org/security/tags" --%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
  <head>
    <title><spring:message code="forum.form"/></title>
  </head>
  <body>
  	<jsp:include page="navBar.jsp" />
    <form:form commandName="forum">
    <form:errors />
      <table>
	        <tr>
	          <th><form:label path="name"><spring:message code="forum.name"/></form:label> *:</th>
	          <td>
	            <form:input path="name" />
	            <form:errors path="name" cssClass="error"/>
	          </td>
	        </tr>
	        <tr>
	          <th><form:label path="description"><spring:message code="forum.description"/></form:label> *:</th>
	          <td>
	            <form:input path="description" />
	            <form:errors path="description" cssClass="error"/>
	          </td>
	        </tr>
	        
        <tr>
          <th>&nbsp;</th>
          <td>
            <input type="submit" value="Save"/>
          </td>
        </tr>
      </table>
    </form:form>
  </body>
</html>