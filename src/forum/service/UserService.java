package forum.service;

import forum.domain.User;

public interface UserService {

	public void registerUser(User user);

}