package forum.service;

import org.springframework.beans.factory.annotation.Autowired;

import forum.dao.UserDao;
import forum.domain.AccountRole;
import forum.domain.User;

public class UserServiceImpl implements UserService {

	
	private UserDao userDao;

	@Autowired
	public UserServiceImpl(UserDao userDao) {
		super();
		this.userDao = userDao;
	}
	
	
	public void registerUser(User user){
		AccountRole acc = new AccountRole("ROLE_USER");
		user.addAccountRole(acc);
		this.userDao.save(user); 
	}


	
}
