package forum.domain;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "topic", uniqueConstraints = { @UniqueConstraint(columnNames = {"category_id" }) })
@NamedQueries({
	@NamedQuery(name = "all-topics", 		query = "from Topic order by created", hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }),
	@NamedQuery(name = "category-topics", 	query = "from Topic where category=:category"),
	@NamedQuery(name = "topic-cat-forum", 	query = "select distinct t from Topic as t inner join fetch t.category c inner join fetch c.forum where t.id = :id"),
})
public class Topic extends IdAndDateEntity{

	private static final long serialVersionUID = -2519207895120008205L;
	private String description;
	private Category category;
	
	@Lob
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@NotNull
	@ManyToOne(optional=false)
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	
}
