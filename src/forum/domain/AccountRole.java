package forum.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import forum.domain.User;

@Entity
@Table(name = "account_role")
public class AccountRole {
	
	private Integer roleId;
	private String role;
	private User user;

	public AccountRole() {
		this.setRole("ROLE_ANONYMOUS");
	}

	public AccountRole(String role) {
		this.setRole(role);
	}
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="accountId")
	public User getUser() {
		return user;
	}

	public void setUser(User User) {
		this.user = User;
	}

}