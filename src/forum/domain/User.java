package forum.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="user")
public class User {

	private Long id;
	private String firstName;
	private String lastName;	
	private String organization;	
	private String title;	
	private String email;	
	private String password;	
	private String passwordDigest;	
	private Date created;
	private boolean version = false;
	private boolean admin = false;
	private boolean enabled = false;
	private List<AccountRole> accountRole = new ArrayList<AccountRole>();
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@NotNull
	@NotEmpty
	@Size(min=2, message="za krotkie imie")
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@NotNull
	@NotEmpty
	@Size(min=2, message="za krotkie nazwisko")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Email
	@Size(max = 64)
	@NotEmpty
	@NotNull
	@Column(length = 64, nullable = false, unique = true)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@NotNull
	@NotEmpty
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public void performCreated(){
		setCreated(new Date());
	}
	
	@Column(nullable=false)
	public boolean isVersion() {
		return version;
	}
	public void setVersion(boolean version) {
		this.version = version;
	}
	@Column(nullable=false)
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	@Column(nullable=false)
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy="user")
	 public List<AccountRole> getAccountRole() {
		return accountRole;
	 }
	 
	 public void setAccountRole(List<AccountRole> accountRole) {
		 for (AccountRole ar : accountRole) {
			 this.addAccountRole(ar);
		 }
	 }
	 
	 public void addAccountRole(AccountRole accountRole) {
		 if (!this.accountRole.contains(accountRole)) {
			 accountRole.setUser(this);
			 this.accountRole.add(accountRole);
		 }
	 }
	public String getPasswordDigest() {
		return passwordDigest;
	}
	public void setPasswordDigest(String passwordDigest) {
		this.passwordDigest = passwordDigest;
	}
	
}
