package forum.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="forum")
public class Forum extends IdAndDateEntity{

	private static final long serialVersionUID = -8958436610553290378L;
	private String name;
	private String description;
	private List<Category> categories = new ArrayList<Category>();
	
	@NotNull
	@NotEmpty
	@Column(unique=true)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Lob
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@OneToMany(mappedBy="forum", cascade=CascadeType.REMOVE, fetch=FetchType.LAZY)
	public List<Category> getCategories() {
		return categories;
	}
	
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	
	
	
}
