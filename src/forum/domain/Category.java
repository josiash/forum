package forum.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
@Entity
@Table(name = "category", uniqueConstraints = { @UniqueConstraint(columnNames = {"forum_id" }) })
@NamedQueries({
	@NamedQuery(name = "all-categories", 	query = "from Category order by created", hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }),
	@NamedQuery(name = "forums-categories", query = "from Category where forum=:forum"),
})

//		@NamedQuery(name = "topic-by-id-fetch-all", query = "select distinct t from Topic as t inner join fetch t.author inner join fetch t.forum f inner join fetch f.category left join fetch t.replies r inner join fetch r.author where t.id=:id", hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }) 
public class Category extends IdAndDateEntity{

	private static final long serialVersionUID = 7715863276984560445L;
	private String name;
	private String description;
	private Forum forum;
	private List<Topic> topics= new ArrayList<Topic>();
	
	@NotNull
	@NotEmpty
	@Size(min=2,max=50)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Lob
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@NotNull
	@ManyToOne(optional=false)
	public Forum getForum() {
		return forum;
	}
	public void setForum(Forum forum) {
		this.forum = forum;
	}
	
	@OneToMany(mappedBy="category", cascade=CascadeType.REMOVE, fetch=FetchType.LAZY)
	public List<Topic> getTopics() {
		return topics;
	}
	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}
	
}
