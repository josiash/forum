package forum.dao;

import java.util.List;

import forum.domain.Forum;


public class ForumDaoImpl extends AbstractHibernateDao<Forum> implements ForumDao {

	public List<Forum> findAll(){
            return super.findAll("from Forum order by id");
    }
	
	public List<Forum> findAll(String order){
        return super.findAll("from Forum order by name " + order);
	}
	
}
