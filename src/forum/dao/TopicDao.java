package forum.dao;

import java.util.List;

import forum.domain.Category;
import forum.domain.Topic;

public interface TopicDao extends TypicalEntityDao<Topic>{

	public List<Topic> findCategoryTopics(Category category);
	
	public Topic findForumCategoryTopic(Long id);

}
