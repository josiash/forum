package forum.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface TypicalEntityDao <E> {

	public E getById(Long id)throws DataAccessException;
	
	public List<E> findAll()throws DataAccessException;
	
	public List<E> findAll(String order)throws DataAccessException;
	
	public void save(E e)throws DataAccessException;
	
	public void deleteById(Long id)throws DataAccessException;
}
