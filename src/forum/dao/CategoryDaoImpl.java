package forum.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import forum.domain.Category;
import forum.domain.Forum;

public class CategoryDaoImpl extends AbstractHibernateDao<Category> implements CategoryDao {

	public List<Category> findAll() throws DataAccessException {
		return super.findAll("from Category order by id");
	}
	
	public List<Category> findAll(String order) throws DataAccessException {
		return super.findAll("from Category order by ?", order);
	}
	
	@SuppressWarnings("unchecked")
	public List<Category> findForumCategories(Forum forum){
		return super.getHibernateTemplate().findByNamedQueryAndNamedParam("forums-categories", "forum", forum);
	}

	@Transactional(readOnly = false)
	public void save(Category category) throws DataAccessException {
		category.markCreated();
		super.save(category);
	}



}
