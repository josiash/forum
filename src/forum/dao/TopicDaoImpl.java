package forum.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import forum.domain.Category;
import forum.domain.Topic;

public class TopicDaoImpl extends AbstractHibernateDao<Topic> implements TopicDao {

	public List<Topic> findAll() throws DataAccessException {
		return super.findAll("from Topic order by id");
	
	}
	
	public List<Topic> findAll(String order) throws DataAccessException {
		return super.findAll("from Topic order by ?", order);
	}

	@SuppressWarnings("unchecked")
	public List<Topic> findCategoryTopics(Category category) {
		return super.getHibernateTemplate().findByNamedQueryAndNamedParam("category-topics", "category", category);
	}
	
	@SuppressWarnings("unchecked")
	public Topic findForumCategoryTopic(Long id) {
		return (Topic) super.getHibernateTemplate().findByNamedQueryAndNamedParam("topic-cat-forum", "id", id).get(0);
	}

	@Transactional(readOnly = false)
	public void save(Topic topic) throws DataAccessException {
		topic.markCreated();
		super.save(topic);
	}

}
