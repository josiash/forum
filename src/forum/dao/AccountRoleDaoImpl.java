package forum.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import forum.domain.AccountRole;

public class AccountRoleDaoImpl extends AbstractHibernateDao<AccountRole> implements
		AccountRoleDao {

	
	@Transactional(readOnly=true)
	public List<AccountRole> findAll() throws DataAccessException {
		return super.findAll("from AccountRole order by roleId");
	}

	public List<AccountRole> findAll(String order) throws DataAccessException {
		return super.findAll("from AccountRole order by ?", order);
	}

}
