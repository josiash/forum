package forum.dao;

import forum.domain.User;

public interface UserDao extends TypicalEntityDao<User>{

	public User getByEmail(String email);
	
}
