package forum.dao;

import forum.domain.Forum;

public interface ForumDao extends TypicalEntityDao<Forum> {

}
