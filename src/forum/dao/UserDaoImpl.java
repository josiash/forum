package forum.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import forum.domain.User;

public class UserDaoImpl extends AbstractHibernateDao<User> implements UserDao{

	public User getByEmail(String email) {
		User user = super.findOne("from User where email=?", email);
		return user;
	}

	public void save(User user) throws DataAccessException{
		user.performCreated();
		super.save(user);
	}
	
	@Transactional(readOnly=true)
	public List<User> findAll(){
		return super.findAll("from User order by firstName");
	}
	
	@Transactional(readOnly=true)
	public List<User> findAll(String order){
		return super.findAll("from User order by ?",order);
	}
	
}
