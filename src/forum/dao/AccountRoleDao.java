package forum.dao;

import forum.domain.AccountRole;

public interface AccountRoleDao extends TypicalEntityDao<AccountRole> {

}
