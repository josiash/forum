package forum.dao;

import java.util.List;

import forum.domain.Category;
import forum.domain.Forum;

public interface CategoryDao extends TypicalEntityDao<Category>{

	public List<Category> findForumCategories(Forum forum);

}
