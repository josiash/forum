package forum.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import forum.dao.CategoryDao;
import forum.dao.TopicDao;
import forum.domain.Topic;

@Controller
@SessionAttributes("topic")
public class TopicController {

	
	CategoryDao categoryDao;
	TopicDao topicDao;

	@Autowired
	public TopicController(CategoryDao categoryDao, TopicDao topicDao) {
		super();
		this.categoryDao = categoryDao;
		this.topicDao = topicDao;
	}

	@RequestMapping(method=RequestMethod.GET, value="topicList")
	public ModelAndView getTopicList(){
		
		ModelAndView mav = new ModelAndView("topicList");
		mav.addObject(topicDao.findAll());
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET , value="topicForm")
	public ModelAndView getTopicForm(
			@RequestParam(value="id", required=false) Long id,
			@RequestParam(value="category_id", required=true) Long categoryId
			){
		Topic topic;
		
		if(id==null){
			topic = new Topic();
			topic.setCategory(categoryDao.getById(categoryId));
		} else {
			topic = topicDao.getById(id);
		}
		ModelAndView mav = new ModelAndView("topicForm");
		mav.addObject(topic);
		return mav;
		
		
		
	}
	
	@RequestMapping(method=RequestMethod.POST,value="topicForm")
	public String saveTopicForm(@ModelAttribute(value="topic") @Valid Topic topic, 
									BindingResult result, SessionStatus status){
		
		if(!result.hasErrors()){
			try{
			topicDao.save(topic);
			status.setComplete();
			return "redirect:category.html?id="+topic.getCategory().getId();
            } catch (DataIntegrityViolationException e) {
                result.rejectValue("name", "DuplicateNameFailure");
            } catch (ConcurrencyFailureException e) {
                result.reject("ConcurrentModificatonFailure",
                        new String[] { "topic" }, null);
            }
		}
		return "topicForm";
	}
	
	@RequestMapping(method=RequestMethod.GET , value="topicView")
	public ModelAndView testHibernateJoin (@RequestParam Long id){
		
		ModelAndView mav = new ModelAndView("topicView");
		Topic topic = (Topic) topicDao.findForumCategoryTopic(id);
		
		mav.addObject("topic", topic);
		
		return mav;
	}
	
}
