package forum.web;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import forum.dao.UserDao;
import forum.domain.User;
import forum.service.UserService;

@Controller
@RequestMapping(value="/user_form")
@SessionAttributes(value="user")
public class UserFormController {

	public UserDao userDao;
	public UserService userService;
	
	@Autowired
	public UserFormController(UserService userService, UserDao userDao) {
		super();
		this.userService = userService;
		this.userDao = userDao;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getForm(@RequestParam(value="id", required = false) Long id ){
		User user = id==null ? new User() : userDao.getById(id); 
		ModelAndView mav = new ModelAndView("userForm").addObject("user", user);
		
		return mav;
	}
		
	
	@RequestMapping(method = RequestMethod.POST)
	public String processSubmit(
			@ModelAttribute("user") @Valid User user,
			BindingResult result, SessionStatus status) {
		
		if (!result.hasErrors()) {
			try {
				userService.registerUser(user);
				status.setComplete();
				return "redirect:home";
			} catch (ConstraintViolationException e) {
				result.addError(new ObjectError("user", e.getMessage()));
			}
		}
		return "userForm";
	}
	
	
	@ExceptionHandler(Exception.class)
	public void handleException(Exception e, HttpServletResponse response)
			throws IOException {
		int code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		String error = "Internal Server Error";
		if (e instanceof ConcurrencyFailureException) {
			code = HttpServletResponse.SC_CONFLICT;
			error = "Concurrent modification failure";
		} else if (e instanceof ConstraintViolationException) {
			code = HttpServletResponse.SC_BAD_REQUEST;
			error = "Constraint violation failure";
		} else if (e instanceof DataIntegrityViolationException) {
			code = HttpServletResponse.SC_CONFLICT;
			error = "Data integrity violation failure";
		} else if (e instanceof DataRetrievalFailureException) {
			code = HttpServletResponse.SC_NOT_FOUND;
			error = "No such resource";
		} else if (e instanceof IllegalArgumentException) {
			code = HttpServletResponse.SC_BAD_REQUEST;
			error = "Invalid request";
		}
		response.sendError(code, error);
	}
	
}
