package forum.web;

import javax.validation.Valid;

import forum.domain.Forum;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.dao.DataIntegrityViolationException;

import forum.dao.CategoryDao;
import forum.dao.ForumDao;

@Controller
@SessionAttributes(value="forum")
public class ForumController {
	
	ForumDao forumDao;
	CategoryDao categoryDao;
	
	@Autowired
	public ForumController(ForumDao forumDao, CategoryDao categoryDao) {
		this.forumDao = forumDao;
		this.categoryDao = categoryDao;
	}
	
	
    @InitBinder
    public void initBinder(WebDataBinder binder) {
/*        binder.registerCustomEditor(Category.class,
                new IdentifiableEntityEditor(this.categoryDao));
        binder.setAllowedFields("category", "name", "description");*/
    }

    @RequestMapping(method = RequestMethod.GET, value = "/forumList")
	public ModelAndView forumlist() {
    	ModelAndView mav = new ModelAndView("forumList");
    	mav.addObject("forumList",forumDao.findAll());
		return mav;
	}
    
    @RequestMapping(method = RequestMethod.GET, value = "/forumList/order/{order}")
	public ModelAndView forumlist(@PathVariable ("order") String order) {
    	ModelAndView mav = new ModelAndView("AJAXforumList");
    	mav.addObject("forumList",forumDao.findAll(order));
		return mav;
	}
    
    @RequestMapping(method = RequestMethod.GET, value = "/forum")
	public ModelAndView forumView(@RequestParam(value="id") Long id) {
		Forum forum = forumDao.getById(id);
    	ModelAndView mav = new ModelAndView("categoryList");
    	mav.addObject("categoryList", categoryDao.findForumCategories(forum));
    	mav.addObject("forum", forum);
		return mav;
	}
    
    @RequestMapping(method = RequestMethod.GET, value = "/forumForm")
    public ModelAndView forumForm(@RequestParam(value = "id", required=false)Long id ){
        Forum forum = id == null ? new Forum() : forumDao.getById(id);
        ModelAndView mav = new ModelAndView("forumForm").addObject("forum",forum);
        return mav;
    }
    
    @RequestMapping(method = RequestMethod.POST, value="/forumForm" )
    public String saveForumForm (@ModelAttribute("forum") @Valid Forum forum, BindingResult result, 
    									SessionStatus status){
    	
    	//TODO check whether error are handled
    	if(!result.hasErrors()){
    		try{
    			forumDao.save(forum);
    			status.setComplete();
    			return "redirect:forumList";
            } catch (DataIntegrityViolationException e) {
                result.rejectValue("name", "DuplicateNameFailure");
            } catch (ConcurrencyFailureException e) {
                result.reject("ConcurrentModificatonFailure",
                        new String[] { "forum" }, null);
            }
    		
    	}
    	return "forumForm";
    }
}
