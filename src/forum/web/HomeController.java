package forum.web;

import java.util.Map;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import forum.dao.UserDao;

@Controller
public class HomeController {
	
	@Autowired
	UserDao user;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@RequestMapping(method = RequestMethod.GET, value = "/home")
	public String showHomePage(Map<String, Object> model) {
		
		logger.debug("There is sth in model: {}",model);
		model.put("users", user.findAll());
		logger.debug("There is sth in model: {}",model);
		return "home";
	}

}
