package forum.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import forum.dao.CategoryDao;
import forum.dao.ForumDao;
import forum.dao.TopicDao;
import forum.domain.Category;

@Controller
@SessionAttributes("category")
public class CategoryController {

	CategoryDao categoryDao;
	ForumDao forumDao;
	TopicDao topicDao;
	
	@Autowired
	public CategoryController(CategoryDao categoryDao, ForumDao forumDao, TopicDao topicDao) {
		this.categoryDao = categoryDao;
		this.forumDao = forumDao;
		this.topicDao = topicDao;
	}
	
    @RequestMapping(method = RequestMethod.GET, value = "/category")
	public ModelAndView categoryView(@RequestParam(value="id") Long id) {
		Category category = categoryDao.getById(id);
    	ModelAndView mav = new ModelAndView("topicList");
    	mav.addObject("topicList", topicDao.findCategoryTopics(category));
    	mav.addObject("category", category);
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET , value="categoryForm")
	public ModelAndView getCategoryForm(
			@RequestParam(value="id", required=false) Long id,
			@RequestParam(value="forum_id", required=true) Long forumId){
		Category category;
		
		if(id==null){
			category = new Category();
			category.setForum(forumDao.getById(forumId));
		} else {
			category = categoryDao.getById(id);
		}
		ModelAndView mav = new ModelAndView("categoryForm");
		mav.addObject(category);
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.POST,value="categoryForm")
	public String saveCategoryForm(@ModelAttribute(value="category") @Valid Category category, 
									BindingResult result, SessionStatus status){
		
		if(!result.hasErrors()){
			try{
			categoryDao.save(category);
			status.setComplete();
			return "redirect:forum.html?id="+category.getForum().getId();
            } catch (DataIntegrityViolationException e) {
                result.rejectValue("name", "DuplicateNameFailure");
            } catch (ConcurrencyFailureException e) {
                result.reject("ConcurrentModificatonFailure",
                        new String[] { "category" }, null);
            }
		}
		return "categoryForm";
	}
}
