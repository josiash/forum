package forum.web;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/uploadFile")
public class FileUploadController {

	@RequestMapping(method = RequestMethod.GET)
	public String fileUpload() {

		return "uploadFile";
	}

	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public String handleUpload(
			@RequestParam(value = "file", required = false) MultipartFile multipartFile,
			HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		String orgName = multipartFile.getOriginalFilename();

		String filePath = request.getServletContext().getRealPath("")+"/users/elo/";
		File dest = new File(filePath);
		dest.mkdirs();
		try {
			multipartFile.transferTo(new File(filePath + orgName));
		} catch (IllegalStateException e) {
			e.printStackTrace();
			return "File uploaded failed:" + orgName;
		} catch (IOException e) {
			e.printStackTrace();
			return "File uploaded failed:" + orgName;
		}
		return "File uploaded:" + orgName;
	}
}